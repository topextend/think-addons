<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Date         : 2021-08-06 19:53:39
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-06-06 12:35:57
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Controller.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2021 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare(strict_types=1);

namespace think\addons;

use think\facade\View;

class Controller extends \think\admin\Controller
{
    /**
     * 请求对象
     * @var Request
     */
    public $request;
    
    // 当前插件标识
    protected $name;

    /**
     * Controller constructor.
     * @param $request
     */
    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->name = $this->getName();
        $addons_view_path = ADDON_PATH . $this->name . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR;
        View::config(['view_path'=> $addons_view_path]);
        $this->initialize();
    }
    
    /**
     * 控制器初始化
     */
    protected function initialize()
    {
        parent::initialize();
    }

    /**
     * 获取插件标识
     * @return mixed|null
     */
    protected function getName()
    {
        $class = get_class($this);
        list(, $name, ) = explode('\\', $class);
        $this->request->addon = $name;
        return $name;
    }

    /**
     * 重写获取器  模型层实例获取
     */
    public function __get($name)
    {
        if (str_prefix($name, 'model')) {
            $class = $this->app->parseClass('model', str_replace('model','', $name));
            if (!class_exists($class)) {
                $class = '\\addons\\' . $this->name . "\\model\\" . str_replace('model','', $name);
            }
        } else {
            throw new \think\exception\HttpException(404, '模型层需引用前缀:model');
        }
        return $class;
    }
}